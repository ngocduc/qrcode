/* App config for images
 */
const images = {
  uni_wallet: require('../assets/uniwallet.png'),
  wallet_bg: require('../assets/walletbg.jpeg'),
  withdraw: require('../assets/withdraw.png'),
  income: require('../assets/income.png'),
  qrcode: require('../assets/qrcode.png'),
};

export default images;
