import { includes } from 'lodash';

export const Constants = {
    ACCESS_TOKEN: 'ACCESS_TOKEN',
    REFRESH_TOKEN: 'REFRESH_TOKEN',
    LOCALE_PERSISTENCE_KEY: 'LANGUAGE',
    ENCRYPT_ALGOROTHMS: 'aes-256-cbc',
    ENCRYPTION_KEY: 'F!@@%12trf@!#D@!%@#4t23fg1',
    IMPORTANT_WALLET: ['UWP6ZUUXGtd5HYBVu8VBkLzy1nya6ouyWT', 'UPyPVSpNfxLtZR79FJ8YV52qmXpQRxHqUM', 'Uj5Nxco2ADWpkr2fk5F87gSQmhMZcKB8iP', 'UkLNAL4EQSf9o9uDvPLKwQC6fSHnbTsR61'],
    MY_WALLET_ADDRESS: 'UgHXvUjsvX1a91PkHwwoR6gUxoXecYBb34'
}

export const checkImportantWallet = (val) => {
    // return Constants.IMPORTANT_WALLET.includes(val);
    return false;
}