/* App config for apis
 */
const ApiConfig = {
  BASE_URL: 'https://explorer.unichain.world/api',
  LOGIN: '/api/login',
  VALIDATE_LOGIN: '/validate/validate_login_mobile',
  NEW_ACCESS_TOKEN: '',
  GET_ALL_TRANSACTION: '/get-transactions-address',
};

export default ApiConfig;
