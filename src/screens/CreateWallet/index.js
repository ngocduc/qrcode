import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Pressable } from 'react-native';
import { Text, useTheme } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { WalletImage } from '../../components/WalletImage';
import { useInput } from '../../components/TextInput';
import { AppButton } from '../../components/AppButton';
import { blackColor } from '../../utils/colorHelper';
import { get } from 'lodash';
import { helpers, SCREEN_WIDTH } from '../../utils/helpers';
import { requestLogin } from '../../redux/actions/loginActions';
import {
  encryptedPrivateKey,
  fetchWalletInfo,
} from '../../redux/actions/walletAction';
import { decrypt, encrypt } from '../../utils/encrypt';
import validateLogin from '../../redux/services/loginUser';
import { walletUtils } from '../../utils/walletHelpers';
import { checkImportantWallet, Constants } from '../../config/constants';
import { Loading } from '../../components/Loading';
import ApiConfig from '../../config/api-config';
import { apiClient } from '../../redux/services/client';

const CreateWallet = (props) => {
  const { navigation, route } = props;
  const [state, setState] = useState({})


  useEffect(() => {
    const data = get(route, 'params.data', '');
    // console.log('adada2323', data);
    try {
      // C55-552104-V04/1000023-08-20
      const [code, stringConvert] = data.split('/');
      const time = stringConvert.substring(stringConvert.length - 8, stringConvert.length);
      const amount = stringConvert.substring(0, stringConvert.length - 8);
      const timeConvert = time + '20';
      // console.log({
      //   code, amount, timeConvert
      // })
      setState({
        code, amount, timeConvert
      })

      // uploadTest({
      //   code, amount, timeConvert
      // })
    } catch (err) {
      console.log(err, '========')
    }
  }, []);
  // partnumber=${code}&date=${timeConvert}&quantity=${amount}`)

  return (
    <View style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 30 }}>
      <View style={{ flex: 1 }}>
        <Text style={styles.text}><Text>Partnumber</Text>: {state.code||'...'}</Text>
        <Text style={styles.text}><Text>Amount</Text>: {state.amount||'...'}</Text>
        <Text style={styles.text}><Text>Date</Text>: {state.timeConvert||'...'}</Text>

      </View>
      <View style={{ flexDirection: 'row' }}>
        <Pressable
          onPress={()=> navigation.goBack()}
          style={{
            height: 50,
            // backgroundColor: '#1890ff',
            borderColor: '#444',
            borderWidth: 1,
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            marginRIght: 10
          }}>
          <Text style={{ fontWeight: '600', fontSize: 20, color: '#444' }}>
            Scan
  </Text>
        </Pressable>
        <Pressable
          onPress={uploadTest}
          style={{
            height: 50,
            backgroundColor: '#1890ff',
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            marginLeft: 10
          }}>
          <Text style={{ color: '#fff', fontWeight: '600', fontSize: 20 }}>
            Upload
  </Text>
        </Pressable>

      </View>


    </View>

  );
};

const styles = StyleSheet.create({
  title: {
    fontWeight: '600',
    textAlign: 'center',
    fontSize: 18,
    marginTop: 10,
  },
  subtitle: {
    textAlign: 'center',
    marginVertical: 7,
    lineHeight: 22,
  },
  label: {
    textAlign: 'center',
    paddingVertical: 7,
    marginTop: 10,
    color: blackColor(0.7),
    lineHeight: 22,
  },
  text: {
    color: '#b8b3c3',
    fontSize: 20,
    fontWeight: '600',
    marginTop: 4,
    marginBottom: 8
  }
});

export default CreateWallet;


const uploadTest = ({ code, timeConvert, amount }) => {
  apiClient.post(`/product?partnumber=${code}&date=${timeConvert}&quantity=${amount}`)
    .then(() => {
      // console.log('========')
      alert("request success")
    })
    .catch(err => {
      console.log('request fail')
    })
}

// const styles = StyleSheet.create({
// })