import React, { useEffect, useRef, useState } from 'react';
import { View, Pressable, Text, StyleSheet } from 'react-native';
import { Button, useTheme } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { Icon } from 'native-base';
import {
  PERMISSIONS,
  check,
  request,
  openSettings,
  RESULTS,
} from 'react-native-permissions';
import { useIsFocused } from '@react-navigation/native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import metrics from '../../config/metrics';
import QRScannerRectView from './component/QRScannerRectView';
import { helpers } from '../../utils/helpers';
import { AppButton } from '../../components/AppButton';

const WalletScanner = (props) => {
  const { colors } = useTheme();
  const refScanner = useRef();
  const isFocused = useIsFocused();
  const { navigation, route } = props;
  const [showFlash, setShowFlash] = useState(false);

  const dispatch = useDispatch();

  const [cameraGranted, setCameraGranted] = useState(false);
  const handleCameraPermission = async () => {
    if (helpers.isIOS) {
      const res = await check(PERMISSIONS.IOS.CAMERA);
      if (res === RESULTS.GRANTED) {
        setCameraGranted(true);
      } else if (res === RESULTS.DENIED) {
        const res2 = await request(PERMISSIONS.IOS.CAMERA);
        res2 === RESULTS.GRANTED
          ? setCameraGranted(true)
          : setCameraGranted(false);
      }
    } else {
      const res = await check(PERMISSIONS.ANDROID.CAMERA);
      if (res === RESULTS.GRANTED) {
        setCameraGranted(true);
      } else if (res === RESULTS.DENIED) {
        const res2 = await request(PERMISSIONS.ANDROID.CAMERA);
        res2 === RESULTS.GRANTED
          ? setCameraGranted(true)
          : setCameraGranted(false);
      }
    }
  };

  useEffect(() => {
    handleCameraPermission();
    setTimeout(() => {
      // onSuccess('C55-552104-V04/1000023-08-20')
    }, 3000)
    // return () => {

    // };
  }, [1]);

  const onSuccess = (e) => {
    try {
      if (e) {
        // alert(JSON.stringify(e));
        if (e.data) {
          // alert(e.data);
          navigation.navigate('CreateWallet', {
            data: e.data,
            // data: 'C55-552104-V04/1000023-08-20'
          });
        }
      }
    } catch (err) {

    }
  };

  if (!cameraGranted) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ textAlign: 'center', lineHeight: 26, fontSize: 15 }}>
          {
            'We need "Camera" permission for this action.\nPlease accept this permission for us'
          }
        </Text>
        <AppButton
          handleAction={() => openSettings()}
          disabled={false}
          text="Go to Settings"
          style={{ alignSelf: 'center', marginTop: 20 }}
        />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      {isFocused && (
        <QRCodeScanner
          cameraStyle={{ height: metrics.screenHeight }}
          containerStyle={{ flex: 1 }}
          ref={refScanner}
          topViewStyle={{ height: 0, flex: 0 }}
          bottomViewStyle={{ height: 0, flex: 0 }}
          onRead={onSuccess}
          flashMode={
            showFlash ?
              RNCamera.Constants.FlashMode.torch :
              RNCamera.Constants.FlashMode.off
          }
        />
      )}
      <View
        style={[
          StyleSheet.absoluteFillObject,
          { backgroundColor: 'transparent' },
        ]}>
        <QRScannerRectView />
      </View>
      <View
        style={{
          // backgroundColor: 'red',
          position: 'absolute', 
          // height: 20, width: 20,
          bottom: 30, right: 30
        }}>
        <Pressable onPress={() => setShowFlash(!showFlash)} >
          <Icon name="lightbulb" type="Foundation" style={{color: 'red'}} />
        </Pressable>
      </View>
    </View>
  );
};

export default WalletScanner;
