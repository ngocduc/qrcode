/**
 * Loading reducer made separate for easy blacklisting
 * Avoid data persist
 */
import createReducer from '../../lib/createReducer';
import * as types from '../actions/types';

const initialState = {
  isAppLoading: true,
  isLoginLoading: false,
};

export const loadingReducer = createReducer(initialState, {
  [types.AUTO_LOGIN_DISABLE_LOADER](state) {
    return { ...state, isAppLoading: false };
  },
  [types.LOGIN_ENABLE_LOADER](state) {
    return { ...state, isLoginLoading: true };
  },
  [types.LOGIN_DISABLE_LOADER](state) {
    return { ...state, isLoginLoading: false };
  },
});
