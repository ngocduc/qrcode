import * as types from './types';

export const fetchWalletInfo = (data) => {
  return {
    type: types.FETCH_WALLET_INFO_SUCCESS,
    data,
  };
};

export const decryptedPrivateKey = (data) => {
  return {
    type: types.DECRYPTED_PRIVATE_KEY,
    data,
  };
};

export const encryptedPrivateKey = (data) => {
  return {
    type: types.ENCRYPTED_PRIVATE_KEY,
    data,
  };
};

export const getWalletResource = (data) => {
  return {
    type: types.GET_WALLET_RESOURCE,
    data,
  };
};

export const handleEditBalanceResource = (data) => {
  return {
    type: types.HANDLE_EDIT_BALANCE,
    data,
  };
};

// lock/unlock coin success
export const handleUnlockUnwSuccess = (data) => {
  return {
    type: types.UNLOCK_UNW_SUCCESS,
    data,
  };
};

export const handleLockUnwSuccess = (data) => {
  return {
    type: types.LOCK_UNW_SUCCESS,
    data,
  };
};
