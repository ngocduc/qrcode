import Unichain from '@uniworld/unichain-js';
import ApiConfig from '../../config/api-config';
import {apiClient} from './client';

//initiate unichainjs
const unichainNetwork = 'https://node-1.unichain.world';
const relayHost = 'https://relay-node-1.unichain.world';

const unichain = new Unichain({
  fullHost: unichainNetwork,
  solidityNode: relayHost,
});

unichain.isConnected((err, data) => {
  // console.log(err, data);
});

/**
 * Send UNW
 * @param {Object} params
 */
export const sendUNW = async (params) => {
  if (params['from_address'] == params['to_address']) {
    //cannot send to your own address
    return 'Fail, cannot return to your own address';
  }
  const amount = parseInt(params['amount']);

  const data = {
    to_address: unichain.address.toHex(params['to_address']),
    owner_address: unichain.address.toHex(params['from_address']),
    amount: amount,
  };
  let unsingedTx = await unichain
    .currentProviders()
    .fullNode.request('wallet/createtransaction', data, 'post');
  // debugger
  let signedTx = await unichain.unx.signTransaction(
    unsingedTx,
    params['private_key'],
    0,
  );
  let res = await unichain.unx.sendRawTransaction(signedTx);
  return res;
};

export const getAllTransactions = (address) => {
  return apiClient.get(`${ApiConfig.GET_ALL_TRANSACTION}/${address}`);
};

/**
 * Check string is unw wallet
 * @param {String} address
 */
export const isUnwAddress = (address) => {
  return Unichain.isAddress(address);
};

/**
 * Get UNW Token issue by Address
 * @param {String} address
 */
export const getUNWTokenByAddress = async (address) => {
  try {
    return await unichain.unx.getTokensIssuedByAddress(address);
  } catch (error) {
    // console.log(error);
    return {};
  }
};

/**
 *
 * @param {Object} data //offset, limit use paging
 */
export const getListUnwTokens = async (data) => {
  try {
    // const results = await  unichain.unx.get(address);
    const limit = (data && data.limit) || 10;
    const offset = (data && data.offset) || 0;
    const results = await unichain.unx.listTokens(limit, offset);
    return results;
  } catch (error) {
    return [];
  }
};
/**
 *
 * @param {String} ID : id of token
 */
export const getTokenByID = async (ID) => {
  try {
    return await unichain.unx.getTokenFromID(ID);
  } catch (error) {
    return null;
  }
};

/**
 *
 * @param {String} address : UNW address
 */
export const getAccountResource = async (address) => {
  // console.log('address === ', address);
  try {
    const results = await unichain.unx.getAccount(address);
    console.log('results ==== ', results);
    //list token
    // let listTokens = [];
    // if (results['asset'].length > 0) {
    //   for (const iterator of results['asset']) {
    //     const token = await getTokenByID(iterator.key);
    //     console.log('111111', token);
    //     iterator.symbol = token['abbr'];
    //     iterator.id = token['id'];
    //     listTokens.push(iterator);
    //   }
    // }

    //unw lock
    let total = 0;
    let expire_time = 0;
    if (results['frozen']) {
      const now = Date.now();
      results['frozen'].forEach((item) => {
        // if (item['expire_time'] > now) {
        total += item['frozen_balance'];
        expire_time = item['expire_time'];
        // }
      });
    }

    //unw vote
    let votes = 0;
    if (results['votes']) {
      results['votes'].forEach((item) => {
        votes += item['vote_count'];
      });
    }

    return {
      status: true,
      balance: results.balance ? results.balance : 0,
      expire_time,
      //   listTokens: listTokens,
      lock: total,
      vote_count: votes,
    };
  } catch (error) {
    // console.log(error);
    return {
      status: false,
      balance: 0,
      expire_time: 0,
      //   listTokens: [],
      lock: 0,
      vote_count: 0,
    };
  }
};

// Buy UNW Token
export const purchaseToken = async ({
  issuerAddress,
  tokenName,
  amount,
  buyerAddress,
  privateKey,
}) => {
  const data = {
    to_address: unichain.address.toHex(issuerAddress),
    owner_address: unichain.address.toHex(buyerAddress),
    amount: toUNW(amount),
    asset_name: unichain.fromUtf8(tokenName),
  };
  // console.log(data)
  try {
    let unsingedTx = await unichain
      .currentProviders()
      .fullNode.request('wallet/participateassetissue', data, 'post');
    let signedTx = await unichain.unx.signTransaction(
      unsingedTx,
      privateKey,
      0,
    );
    return await unichain.unx.sendRawTransaction(signedTx);
    // console.log(res);
    // return res;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Get unw reward
 * @param {String} address
 */
export const getReward = async (address) => {
  try {
    const res = await unichain.unx.getReward(address);
    return {
      status: true,
      data: res,
    };
  } catch (error) {
    return {
      status: false,
      error: error,
    };
  }
};
/**
 * WithDraw reward
 * @param {String} address
 * @param {String} witnessPrivateKey
 */
export const withdrawReward = async ({address, privateKey}) => {
  try {
    let unsingedTx = await unichain.transactionBuilder.withdrawBlockRewards(
      address,
    );
    let signedTx = await unichain.unx.signTransaction(
      unsingedTx,
      privateKey,
      0,
    );
    let res = await unichain.unx.sendRawTransaction(signedTx);
    return {
      status: true,
      data: res,
    };
  } catch (error) {
    console.log(error);
    return {
      status: false,
      error: error,
    };
  }
};

const toUNW = (ginza) => ginza * 1000000;
export const lockUNW = async ({ginza, voter_address, voter_privateKey}) => {
  try {
    let unsingedTx = await unichain.transactionBuilder.freezeBalance(
      toUNW(ginza),
      3,
      'BANDWIDTH',
      voter_address,
    );

    // console.log("lockBalance unsingedTx :", unsingedTx);
    let signedTx = await unichain.unx.signTransaction(
      unsingedTx,
      voter_privateKey,
      0,
    );
    return await unichain.unx.sendRawTransaction(signedTx);
  } catch (error) {
    // console.log(error);
    return {
      status: false,
      message: error,
    };
  }
};

export const unlockUnw = async ({
  ownerAddress,
  privateKey,
  type = 'BANDWIDTH',
}) => {
  try {
    let unsingedTx = await unichain.transactionBuilder.unfreezeBalance(
      type,
      ownerAddress,
    );
    // console.log('lockBalance unsingedTx :', unsingedTx)
    let signedTx = await unichain.unx.signTransaction(
      unsingedTx,
      privateKey,
      0,
    );
    let res = await unichain.unx.sendRawTransaction(signedTx);
    // console.log('coin transaction: ', res)

    return res;
  } catch (error) {
    console.log(error);
    return {
      result: false,
      error: error,
    };
  }
};
