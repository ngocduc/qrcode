/**
 *  Redux saga class init
 */
import {all, takeLatest} from 'redux-saga/effects';
import * as types from '../actions/types';
import {loginAsync, autoLoginAsync} from './loginSaga';

export default function* watch() {
  yield all([takeLatest(types.AUTO_LOGIN_REQUEST, autoLoginAsync)]);
}
