import { put, delay } from 'redux-saga/effects';

import * as loginActions from '../actions/loginActions';

export function* autoLoginAsync() {
  yield put(loginActions.enableAutoLoader());

  yield delay(1000);
  yield put(loginActions.disableAutoLoader());
  
}

