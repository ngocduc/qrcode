import React, {useRef} from 'react';
import {View} from 'react-native';
import {NavigationContainer, Theme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

import {navigationRef} from './NavigationService';

import Home from '../screens/Home';
import StartUp from '../screens/StartUp';

import ThemeController from '../components/ThemeController/ThemeController';
import {StatusBar, Pressable, Text} from 'react-native';
import ImportWallet from '../screens/ImportWallet';
import CreateWallet from '../screens/CreateWallet';
import GenerateWalletInfo from '../screens/GenerateWalletInfo';
import WalletGreeting from '../screens/WalletGreeting';
import {Icon} from 'native-base';
import {blackColor} from '../utils/colorHelper';
import TransactionHistory from '../screens/TracsationHistory';
import LogoutController from '../components/LogoutController';
import WalletScanner from '../screens/WalletScanner';
import {get} from 'lodash';

const Stack = createStackNavigator();
const AuthStack = createStackNavigator();
const LoggedInStack = createStackNavigator();

const homeOptions = {
  title: 'Home',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  headerRight: () => <ThemeController />,
};

const AuthNavigator = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerTitleStyle: {fontWeight: 'bold', textAlign: 'center'},
        headerBackTitleVisible: false,
        headerLeft: (props) => {
          return (
            <Pressable style={{paddingHorizontal: 5, width: 40}} {...props}>
              <Icon
                name="chevron-back"
                style={{fontSize: 34, color: blackColor(1)}}
              />
            </Pressable>
          );
        },
        headerRight: () => <View style={{width: 40}} />,
        headerStyle: {
          borderBottomWidth: 0,
          elevation: 0,
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          },
        },
      }}>
      <Stack.Screen
        name="ImportWallet"
        component={ImportWallet}
        options={{
          title: 'Scan QRCode',
          headerLeft: () => <View style={{width: 40}} />,
          // animationTypeForReplace: isLoggedIn ? 'push' : 'pop',
          // headerRight: () => <ThemeController />,
        }}
      />
      <Stack.Screen
        name="CreateWallet"
        component={CreateWallet}
        options={{
          title: 'Nội dung barcode',
          // headerRight: () => <ThemeController />,
        }}
      />
      <Stack.Screen
        name="WalletGreeting"
        component={WalletGreeting}
        options={{
          title: 'Create New Wallet',
          // headerRight: () => <ThemeController />,
        }}
      />
      <Stack.Screen
        name="GenerateWalletInfo"
        component={GenerateWalletInfo}
        options={{
          title: 'Wallet Information',
          // headerRight: () => <ThemeController />,
        }}
      />
    </AuthStack.Navigator>
  );
};

const LoggedInNavigator = () => (
  <LoggedInStack.Navigator
    screenOptions={{
      headerTitleStyle: {fontWeight: 'bold', textAlign: 'center'},
      headerBackTitleVisible: false,
      headerLeft: (props) => {
        return (
          <Pressable style={{paddingHorizontal: 5, width: 40}} {...props}>
            <Icon
              name="chevron-back"
              style={{fontSize: 34, color: blackColor(1)}}
            />
          </Pressable>
        );
      },
      headerRight: () => <View style={{width: 40}} />,
      headerStyle: {
        borderBottomWidth: 0,
        elevation: 0,
        shadowRadius: 0,
        shadowOffset: {
          height: 0,
        },
      },
    }}>
    <Stack.Screen
      name="Home"
      component={Home}
      options={{
        title: `Uni's Wallet`,
        headerLeft: () => <View style={{width: 40}} />,
        headerRight: () => <LogoutController />,
      }}
    />
    <Stack.Screen
      name="TransactionHistory"
      component={TransactionHistory}
      options={{
        title: 'Transactions',
      }}
    />
    <Stack.Screen
      name="WalletScanner"
      component={WalletScanner}
      options={{
        title: 'Wallet Scanner',
      }}
    />
  </LoggedInStack.Navigator>
);

const RootNavigation = (props) => {
  const {theme} = props;
  const isLoggedIn = useSelector((state) =>
    get(state, 'walletReducer.walletInfo.encryptedPrivateKey', null),
  );
  const isAppLoading = useSelector(
    (state) => state.loadingReducer.isAppLoading,
  );

  if (isAppLoading) {
    return <StartUp />;
  }

  return (
    <NavigationContainer ref={navigationRef} theme={theme}>
      <StatusBar
        barStyle={theme.dark ? 'light-content' : 'dark-content'}
        backgroundColor={theme.dark ? 'black' : 'white'}
      />

      <Stack.Navigator headerMode="none">
        {isLoggedIn ? (
          <Stack.Screen
            name="Home"
            component={LoggedInNavigator}
            options={homeOptions}
          />
        ) : (
          <Stack.Screen
            name="AuthStack"
            component={AuthNavigator}
            // options={{
            //   headerRight: () => <ThemeController />,
            // }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
