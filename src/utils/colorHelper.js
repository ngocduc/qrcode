export const blackColor = (opa) => {
  return `rgba(0, 0, 0, ${opa})`;
};

export const whiteColor = (opa) => {
  return `rgba(255, 255, 255, ${opa})`;
};
