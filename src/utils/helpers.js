import {Platform, Linking, Dimensions} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {
  getBottomSpace,
  getStatusBarHeight,
  isIphoneX,
} from 'react-native-iphone-x-helper';
import * as _ from 'lodash';
import Clipboard from '@react-native-community/clipboard';
import Toast from 'react-native-simple-toast';
import ExactMath from 'exact-math';

const isIOS = Platform.OS === 'ios';
const isTablet = DeviceInfo.isTablet();
const isIpX = isIphoneX();
const bottomSpace = getBottomSpace();
const statusBarHeight = getStatusBarHeight();

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

const validatePhone = (phone) => {
  const regexPhone = new RegExp(
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
  );
  return regexPhone.test(phone);
};

const validatePwd = (pwd) => {
  const regexPassword = new RegExp(
    /^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$/,
  );
  return regexPassword.test(pwd);
};

const removeHexPrefix = (key) => {
  return key.slice(0, 2) == '0x' ? key.slice(2) : key;
};

const validateEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const convertPhone = (phone) => {
  const temp = '84';
  return '+'.concat(temp.concat(phone.replace(/^0+/, '').trim()));
};

const elevationShadowStyle = (elevation) => {
  return {
    elevation,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 0.5 * elevation},
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation,
  };
};

const isEmpty = (str) => {
  return _.isEmpty(str.trim());
};

const _handleLink = async (url) => {
  const supported = await Linking.canOpenURL(url);
  if (supported) {
    await Linking.openURL(url);
  } else {
    alert(`Địa chỉ này không đúng: ${url}`);
  }
};

const convertDate = (time) => {
  const temp = time.split('T');
  const new_time = temp[0].split('-');
  if (new_time.length === 3) {
    return new_time[2] + '/' + new_time[1] + '/' + new_time[0];
  } else {
    return temp[0];
  }
};

const getCurrentDate = () => {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0');
  const yyyy = today.getFullYear();
  return dd + '/' + mm + '/' + yyyy;
};

const convertTime = (seconds) => {
  const m = parseInt(seconds / 60);
  const s = parseInt(seconds % 60);
  return (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s);
};

const formatNumber = (num) => {
  return ExactMath.add(num / 1000000, 0);
};

const handleCopyToClipboard = (content = '', toast = 'Copied') => {
  Clipboard.setString(content);
  Toast.showWithGravity(toast, Toast.SHORT, Toast.BOTTOM);
};

const convertMiliSecToTime = (time) => {
  let day = parseInt(time / (24 * 60 * 60));
  let hour = parseInt((time - 24 * 60 * 60 * day) / (60 * 60));
  let minute = parseInt((time - 24 * 60 * 60 * day - 60 * 60 * hour) / 60);
  let second = time - 24 * 60 * 60 * day - 60 * 60 * hour - minute * 60;
  if (day > 0) {
    day = appendOx(day) + 'd:';
  } else {
    day = '';
  }
  if (hour > 0) {
    hour = appendOx(hour) + 'h:';
  } else {
    hour = '';
  }
  if (minute > 0) {
    minute = appendOx(minute) + 'm:';
  } else {
    minute = '';
  }
  if (second > 0) {
    second = appendOx(second) + 's';
  } else {
    second = '';
  }
  return `${day}${hour}${minute}${second}`.trim();
};

const appendOx = (val) => {
  if (val > 0 && val < 10) {
    return '0' + val;
  } else if (val >= 10) {
    return val;
  } else {
    return '';
  }
};

export const helpers = {
  isTablet,
  isIOS,
  isIpX,
  validatePwd,
  validatePhone,
  elevationShadowStyle,
  isEmpty,
  validateEmail,
  convertPhone,
  _handleLink,
  convertDate,
  getCurrentDate,
  convertTime,
  formatNumber,
  handleCopyToClipboard,
  bottomSpace,
  statusBarHeight,
  removeHexPrefix,
  convertMiliSecToTime,
};
